# ElderScrollsLegends

Displays card data from the elderscrollslegends api

## API

This project uses the [https://api.elderscrollslegends.io/](https://api.elderscrollslegends.io/) api


### Setup

```
npm install
```

### Run

```
npm run serve
```

### Build

```
npm run build
```